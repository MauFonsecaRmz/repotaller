let clientesObtenidos;

function getClientes(){
    var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function(){

        if(this.readyState==4 && this.status==200){
            console.log(request.responseText);

            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    }

    request.open("GET",url,true);
    request.send();
}

function procesarClientes() {
    
    let rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    let JSONClientes = JSON.parse(clientesObtenidos);

    let divTabla = document.getElementById("divTabla");
    let tabla = document.createElement("table");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for(let x=0; x < JSONClientes.value.length; x++){
        let nuevaFila = document.createElement("tr");
        
        let columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[x].ContactName;
        nuevaFila.append(columnaNombre);

        let columnaCiudad = document.createElement("td");
        columnaCiudad.innerText =  JSONClientes.value[x].City;
        nuevaFila.append(columnaCiudad);

        let columnaTelefono = document.createElement("td");
        columnaTelefono.innerText =  JSONClientes.value[x].Phone;
        nuevaFila.append(columnaTelefono);

        let columnaBandera = document.createElement("td");
        let imagenBandera = document.createElement("img");

        imagenBandera.classList.add("flag");
        if(JSONClientes.value[x].Country == "UK"){
            imagenBandera.src = rutaBandera+"United-Kingdom.png";
        }else{
            imagenBandera.src = rutaBandera+JSONClientes.value[x].Country+".png";
        }
        columnaBandera.appendChild(imagenBandera);
        nuevaFila.append(columnaBandera);

        tabla.appendChild(nuevaFila);
    }

    divTabla.append(tabla);
}