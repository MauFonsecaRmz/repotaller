let productosObtenidos;

function getProductos(){
    let url= "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    let request = new XMLHttpRequest();

    request.onreadystatechange = function(){

        if(this.readyState==4 && this.status==200){
            console.log(request.responseText);
            productosObtenidos = request.responseText;
            procesarProductos();
        }
    }

    request.open("GET",url,true);
    request.send();
}

function procesarProductos() {
    var JSONProductos = JSON.parse(productosObtenidos);
    alert(JSONProductos.value[0].ProductName);

    for (let i = 0; i < JSONProductos.value.length; i++) {
       console.log(JSONProductos.value[i].ProductName);         
    }

    let divTabla = document.getElementById("divTabla");
    let tabla = document.createElement("table");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for(let x=0; x < JSONProductos.value.length; x++){
        let nuevaFila = document.createElement("tr");
        
        let columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONProductos.value[x].ProductName;
        nuevaFila.append(columnaNombre);

        let columnaPrecio = document.createElement("td");
        columnaPrecio.innerText =  JSONProductos.value[x].UnitPrice;
        nuevaFila.append(columnaPrecio);

        let columnaStock = document.createElement("td");
        columnaStock.innerText = JSONProductos.value[x].UnitsInStock;
        nuevaFila.append(columnaStock);

        tabla.appendChild(nuevaFila);
    }

    divTabla.append(tabla);
}